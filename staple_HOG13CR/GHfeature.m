function out = GHfeature(img)
    [M,O] = gradHistMex('gradMagOri',img);
    out = gradHistMex('gradHist',M,O);
end