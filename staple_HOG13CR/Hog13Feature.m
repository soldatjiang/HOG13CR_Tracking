function out = Hog13Feature(img)
    hog36 = hog(img, 4, 9);
    [h, w, ~] = size(hog36);
    out = zeros(h, w, 13, 'single');
    for b=1:9
        out(:,:,b)=hog36(:,:,b)+hog36(:,:,b+9)+hog36(:,:,b+18)+hog36(:,:,b+27);
    end
    for n=1:4
        for b=1:9
            out(:,:,9+n)=out(:,:,9+n)+hog36(:,:,(n-1)*9+b);
        end
    end
end