function out = getCRFeature(im_patch, cf_response_size, tg_hist, cxt_hist)
    h = cf_response_size(1);
    w = cf_response_size(2);
    out = zeros(h, w, 3, 'single');
    im_patch = mexResize(im_patch, [h, w] ,'auto');
    out(:,:,1) = get_colour_map(im_patch, tg_hist, cxt_hist{1});
    out(:,:,2) = get_colour_map(im_patch, tg_hist, cxt_hist{2});
    out(:,:,3) = get_colour_map(im_patch, tg_hist, cxt_hist{3});
end

