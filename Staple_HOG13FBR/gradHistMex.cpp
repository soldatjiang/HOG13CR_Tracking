#include "mex.h"
#include <math.h>
#include <string.h>
#define PI 3.1415926
#define ACOS_COEF 1000
#define FLOAT_ZERO 0.0000001f
#define GH_BIN_NUM 9
#define GH_CELL_SIZE 4

float* acostable()
{
    static float table[ACOS_COEF];
    float* a1 = table;
    int i;
    for(i=0;i<ACOS_COEF;i++)
        table[i]=acos(1.0*i/ACOS_COEF);
    return a1;
}

void gradMagOri(int nl, mxArray* pl[], int nr, mxArray* pr[])
{
    if(nl!=2) mexErrMsgTxt("Incorrect number of outputs!");
    int nDims;
    const mwSize* dims;
    mwSize outDims[3];
    int h, w, d;
    float *img, *mag;
    unsigned char* ori;
    nDims = mxGetNumberOfDimensions(pr[0]); 
    dims = mxGetDimensions(pr[0]);
    h=dims[0]; w=dims[1]; d=(nDims==2) ? 1 : dims[2];
    //为输出的幅值和相角分配空间
    img = (float*)mxGetPr(pr[0]);
    mag = (float*)mxCalloc(h*w*d, sizeof(float));
    ori = (unsigned char*)mxCalloc(h*w*d, sizeof(unsigned char));
    
    //创建输出矩阵，设置维度
    pl[0] = mxCreateNumericMatrix(0,0,mxSINGLE_CLASS,mxREAL);
    pl[1] = mxCreateNumericMatrix(0,0,mxUINT8_CLASS,mxREAL);
    outDims[0] = h; outDims[1] = w; outDims[2] = 1;
    mxSetData(pl[0],mag); mxSetDimensions(pl[0],outDims,3);
    mxSetData(pl[1],ori); mxSetDimensions(pl[1],outDims,3);
    
    int i,j;
    int line_begin, offset;
    float mag_tmp, ori_tmp;
    float dx, dy, dx_abs;
    float* acos_table = acostable();
    for(i=1;i<w-1;i++)
    {
        line_begin = i*h;
        for(j=1;j<h-1;j++)
        {
            offset = line_begin + j;
            dx = img[offset+h] - img[offset-h];
            dy = img[offset+1] - img[offset-1];
            mag_tmp = (float)(sqrt(dx*dx+dy*dy));
            mag[offset] = mag_tmp;
            
            if(mag_tmp>FLOAT_ZERO)
            {
                dx_abs = dx>0 ? dx : (-dx);
                ori_tmp = acos_table[(int)(dx_abs/mag_tmp*ACOS_COEF)];
                if(dx*dy<0)
                    ori_tmp = PI - ori_tmp;
                ori[offset] = (int)(ori_tmp/PI*180);
            }
        }
    }
}

void gradHist(int nl, mxArray* pl[], int nr, mxArray* pr[])
{
    if(nl!=1) mexErrMsgTxt("Incorrect number of outputs!");
    int nDims;
    const mwSize* dims;
    mwSize outDims[3];
    int h, w, d;
    int h_gh, w_gh;
    float *mag, *out;
    unsigned char* ori;
    float channel_max[GH_BIN_NUM];
    nDims = mxGetNumberOfDimensions(pr[0]); 
    dims = mxGetDimensions(pr[0]);
    h=dims[0]; w=dims[1]; d=(nDims==2) ? 1 : dims[2];
    h_gh = h/GH_CELL_SIZE; w_gh = w/GH_CELL_SIZE;
    mag = (float*)mxGetPr(pr[0]);
    ori = (unsigned char*)mxGetPr(pr[1]);
    out = (float*)mxCalloc(h_gh*w_gh*GH_BIN_NUM, sizeof(float));
    
    pl[0] = mxCreateNumericMatrix(0,0,mxSINGLE_CLASS,mxREAL);
    outDims[0] = h_gh; outDims[1] = w_gh; outDims[2] = GH_BIN_NUM;
    mxSetData(pl[0],out); mxSetDimensions(pl[0],outDims,3);

    int i, j, k;
    int bin_size = 180/GH_BIN_NUM;
    int offset;
    float mag_tmp;
    int ori_tmp, channel;
    int cell_pixel_num = GH_CELL_SIZE*GH_CELL_SIZE;
    int cell_pos[GH_CELL_SIZE*GH_CELL_SIZE];
    float nor_ratio = 1.0f/bin_size/cell_pixel_num;
    unsigned char table_bin1[181];
    unsigned char table_bin2[181];
    unsigned char table_weight1[181];
    unsigned char table_weight2[181];
    
    //准备投票表
    for(i=0;i<180;i++)
    {
        table_bin1[i] = i/bin_size;
        if(table_bin1[i]==GH_BIN_NUM-1)
            table_bin2[i]=0;
        else
            table_bin2[i] = table_bin1[i]+1;
        table_weight1[i] = bin_size - (i%bin_size);
        table_weight2[i] = bin_size - table_weight1[i];
    }
    table_bin1[180] = GH_BIN_NUM-1;
    table_bin2[180] = 0;
    table_weight1[180] = bin_size;
    table_weight2[180] = 0;
    for(i=0; i<GH_CELL_SIZE; i++)
        for(j=0; j<GH_CELL_SIZE; j++)
            cell_pos[i*GH_CELL_SIZE+j]=i*h+j;
    
    for(i=0; i<w_gh; i++)
        for(j=0; j<h_gh; j++)
        {
            offset = GH_CELL_SIZE*h*i+GH_CELL_SIZE*j;
            for(k=0; k<GH_CELL_SIZE*GH_CELL_SIZE; k++)
            {
                if(offset+cell_pos[k]>=h*w)
                    mexErrMsgTxt("Limit exceeded!");
                mag_tmp = mag[offset+cell_pos[k]];
                ori_tmp = ori[offset+cell_pos[k]];
                channel = table_bin1[ori_tmp];//通过投票表查出该梯度方向在GH特征中所属通道
                //if(channel>=9)
                //    printf("channel=%d\n",channel);
                out[i*h_gh+j+channel*h_gh*w_gh] += nor_ratio*table_weight1[ori_tmp]*mag_tmp;
                channel = table_bin2[ori_tmp];
                //if(channel>=9)
                //    printf("channel=%d\n",channel);
                out[i*h_gh+j+channel*h_gh*w_gh] += nor_ratio*table_weight2[ori_tmp]*mag_tmp;
            }
        }
    
    for(i=0; i<9; i++)
        channel_max[i] = 0;
    
    for(i=0; i<GH_BIN_NUM; i++)
        for(j=0; j<h_gh*w_gh; j++)
        {
            if(out[j+i*h_gh*w_gh]>channel_max[i])
                channel_max[i] = out[j+i*h_gh*w_gh];
        }
    
    for(i=0; i<GH_BIN_NUM; i++)
        for(j=0; j<h_gh*w_gh; j++)
        {
            out[j+i*h_gh*w_gh] /= channel_max[i];
        }
}

void mexFunction(int nl, mxArray* pl[], int nr, mxArray* pr[])
{
    int f;char action[1024]; f=mxGetString(pr[0],action,1024); nr--; pr++;
    if(f) mexErrMsgTxt("Failed to get action.");
    else if(!strcmp(action,"gradMagOri")) gradMagOri(nl,pl,nr,pr);
    else if(!strcmp(action,"gradHist")) gradHist(nl,pl,nr,pr);
}