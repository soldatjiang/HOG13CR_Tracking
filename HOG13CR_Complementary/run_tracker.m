base_path = 'D:\JiangShan\data_seq\otb100\Benchmark\';

params = readParams('params.txt');
video_path = choose_video(base_path);
if isempty(video_path), return, end  %user cancelled
[params.img_files, pos, target_sz, ground_truth, video_path] = ...
	load_video_info(video_path);

params.img_path = '';

im = imread(params.img_files{1});
% grayscale sequence? --> use 1D instead of 3D histograms
if(size(im,3)==1)
    params.grayscale_sequence = true;
end

params.init_pos = floor(pos) + floor(target_sz/2);
params.target_sz = target_sz;

[params, bg_area, fg_area, area_resize_factor] = initializeAllAreas(im, params);
%if params.visualization
%		params.videoPlayer = vision.VideoPlayer('Position', [100 100 [size(im,2), size(im,1)]+30]);
%	end
% in runTracker we do not output anything because it is just for debug
params.fout = -1;
results = trackerMain(params, im, bg_area, fg_area, area_resize_factor);
