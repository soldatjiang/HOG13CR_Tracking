function [tg_hist_new, cxt_hist_new] = update_histogram_model3(im, pos, target_sz, update_rate, new_model, tg_hist, cxt_hist)
    target_patch = crop_patch(im, pos, target_sz, 1);
    [h, w, d] = size(target_patch);
    patch_array = reshape(double(target_patch), h*w, d);
    bin_indices = floor(patch_array/16)+1;
    [rm, cm] = ndgrid(1:h, 1:w);
    rm = rm - floor(h/2);
    cm = cm - floor(w/2);
    rm = rm / h * 2;
    cm = cm / w * 2;
    tg_mask =  1 - rm.^2 - cm.^2;
    tg_mask(find(tg_mask<0))=0;
    
    if d==3
        if new_model
            tg_hist_new = accumarray(bin_indices, tg_mask(:), [16 16 16])/sum(tg_mask(:));
        else
            tg_hist_new = (1 - update_rate) * tg_hist + update_rate * accumarray(bin_indices, tg_mask(:), [16 16 16])/sum(tg_mask(:));
        end

        context_patch = crop_patch(im, pos, target_sz, 2);
        [h, w, d] = size(context_patch);
        patch_array = reshape(double(context_patch), h*w, d);
        bin_indices = floor(patch_array/16)+1;
        mask = ones([h,w]);
        cxt2_hist = accumarray(bin_indices, mask(:), [16 16 16])/sum(mask(:));

        context_patch = crop_patch(im, pos, target_sz, 3);
        [h, w, d] = size(context_patch);
        patch_array = reshape(double(context_patch), h*w, d);
        bin_indices = floor(patch_array/16)+1;
        mask = ones([h,w]);
        cxt3_hist = accumarray(bin_indices, mask(:), [16 16 16])/sum(mask(:));

        context_patch = crop_patch(im, pos, target_sz, 4);
        [h, w, d] = size(context_patch);
        patch_array = reshape(double(context_patch), h*w, d);
        bin_indices = floor(patch_array/16)+1;
        mask = ones([h,w]);
        cxt4_hist = accumarray(bin_indices, mask(:), [16 16 16])/sum(mask(:));
    else
        if new_model
            tg_hist_new = accumarray(bin_indices, tg_mask(:), [16 1])/sum(tg_mask(:));
        else
            tg_hist_new = (1 - update_rate) * tg_hist + update_rate * accumarray(bin_indices, tg_mask(:), [16 1])/sum(tg_mask(:));
        end

        context_patch = crop_patch(im, pos, target_sz, 2);
        [h, w, d] = size(context_patch);
        patch_array = reshape(double(context_patch), h*w, d);
        bin_indices = floor(patch_array/16)+1;
        mask = ones([h,w]);
        cxt2_hist = accumarray(bin_indices, mask(:), [16 1])/sum(mask(:));

        context_patch = crop_patch(im, pos, target_sz, 3);
        [h, w, d] = size(context_patch);
        patch_array = reshape(double(context_patch), h*w, d);
        bin_indices = floor(patch_array/16)+1;
        mask = ones([h,w]);
        cxt3_hist = accumarray(bin_indices, mask(:), [16 1])/sum(mask(:));

        context_patch = crop_patch(im, pos, target_sz, 4);
        [h, w, d] = size(context_patch);
        patch_array = reshape(double(context_patch), h*w, d);
        bin_indices = floor(patch_array/16)+1;
        mask = ones([h,w]);
        cxt4_hist = accumarray(bin_indices, mask(:), [16 1])/sum(mask(:));
    end
    
    if new_model
         cxt_hist_new = {cxt2_hist, cxt3_hist, cxt4_hist};
    else
         cxt2_hist_new = (1 - update_rate) * cxt_hist{1} + update_rate * cxt2_hist;
         cxt3_hist_new = (1 - update_rate) * cxt_hist{2} + update_rate * cxt3_hist;  
         cxt4_hist_new = (1 - update_rate) * cxt_hist{3} + update_rate * cxt4_hist;
         cxt_hist_new = {cxt2_hist_new, cxt3_hist_new, cxt4_hist_new};
    end
end